package com.udara.clientappspring;

import com.udara.clientappspring.service.inputservice.ConsoleInputService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;

/**
 * Client Application
 */
@SpringBootApplication
@ImportResource("classpath:spring-context.xml")
@PropertySource("classpath:application.properties")
public class ClientApp {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(ClientApp.class, args);
        ConsoleInputService inputService = applicationContext.getBean(ConsoleInputService.class);
        inputService.prompt();
    }
}