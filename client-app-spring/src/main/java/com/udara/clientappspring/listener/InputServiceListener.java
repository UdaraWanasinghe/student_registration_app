package com.udara.clientappspring.listener;

public interface InputServiceListener<T> {
    void onInputSuccess(T t);
}
