package com.udara.clientappspring.service.inputservice;

import com.udara.clientapp.listener.ClientServiceListener;
import com.udara.clientapp.service.clientservice.ClientServiceInterface;
import com.udara.clientappspring.listener.InputServiceListener;
import com.udara.common.codec.JsonCodec;
import com.udara.common.model.Reply;
import com.udara.common.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ConsoleInputService implements InputServiceInterface, InputServiceListener<User>, ClientServiceListener {
    private static Logger logger = LogManager.getLogger(ConsoleInputService.class);

    private ClientServiceInterface clientService;

    public ConsoleInputService(ClientServiceInterface clientService) {
        this.clientService = clientService;
    }

    @Override
    public String[] read() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("----------------------------------");
        System.out.println("--------Input student data--------");
        System.out.println("----------------------------------");

        System.out.println("Index no: ");
        String indexNo = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("Name    : ");
        String name = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("Email   : ");
        String email = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("DOB     : ");
        String dob = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("Address : ");
        String address = scanner.nextLine();
        System.out.println("----------------------------------");

        return new String[]{
                indexNo,
                name,
                email,
                dob,
                address
        };
    }

    public void prompt() {
        try {
            System.out.println("Connecting to client");
            clientService.connect();
            System.out.println("Connected to client");
            String[] data = read();
            String index = data[0];
            String name = data[1];
            String email = data[2];
            String dob = data[3];
            String address = data[4];
            User user = new User();
            user.setId(index);
            user.setUsername(name);
            user.setEmail(email);
            user.setDob(dob);
            user.setAddress(address);

            if (isValid(user)) {
                System.out.println("Inputs are valid");
                this.onInputSuccess(user);
            } else {
                System.out.println("Invalid inputs");
                prompt();
            }
        } catch (IOException e) {
            logger.info(e);
        }
    }

    public boolean isValid(User user) {
        boolean isValid = true;
        Pattern emailPattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String id = user.getId();
        String name = user.getUsername();
        String email = user.getEmail();
        String dob = user.getDob();
        String address = user.getAddress();

        if (id == null) {
            isValid = false;
            logger.error("Index no can't be empty");
        } else if (id.length() != 7) {
            isValid = false;
            logger.error("Index no is invalid");
        }

        if (name == null) {
            isValid = false;
            logger.error("Name can't be empty");
        } else if (name.length() < 5) {
            isValid = false;
            logger.error("Username must be more than 4 characters in length");
        }

        if (email == null || email.isEmpty()) {
            isValid = false;
            logger.error("Email can't be empty");
        } else if (!emailPattern.matcher(user.getEmail()).find()) {
            isValid = false;
            logger.error("Invalid email address");
        }

        if (user.getDob() == null) {
            isValid = false;
            logger.error("Dob is required");
        } else {
            try {
                simpleDateFormat.parse(dob);
            } catch (ParseException e) {
                isValid = false;
                logger.error("Dob is invalid");
            }
        }
        if (address == null || address.isEmpty()) {
            isValid = false;
            logger.error("Address can't be empty");
        }
        return isValid;
    }

    @Override
    public void onInputSuccess(User user) {
        String str = JsonCodec.encodeJson(user);
        System.out.println("Json encoded: " + str);
        try {
            clientService.write(str.getBytes());
            System.out.println("client write done");
            String string = clientService.read();
            Reply reply = JsonCodec.decodeJson(string, Reply.class);
            clientService.close();
            logger.info(reply.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void onReply(byte[] data) {
        Reply reply = JsonCodec.decodeJson(data, Reply.class);
        logger.info(reply.getMessage());
    }

    @Override
    public void onError(String err) {
        logger.debug("Connection timeout triggered");
    }
}
