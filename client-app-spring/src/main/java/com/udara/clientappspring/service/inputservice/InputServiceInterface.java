package com.udara.clientappspring.service.inputservice;

public interface InputServiceInterface {
    public String[] read();
}
