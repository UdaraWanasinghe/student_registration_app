package com.udara.common.codec;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class JsonCodec {

    private JsonCodec() {
    }

    /**
     * When json data is given this decodes data to the given class type
     *
     * @param data data to decode
     * @param cls  class type of the output
     * @param <T>  class type of the output
     * @return java object of given data
     */
    public static <T> T decodeJson(byte[] data, Class<T> cls) {
        Gson gson = new Gson();
        String str = new String(data);
        return gson.fromJson(str, cls);
    }

    public static <T> T decodeJson(String data, Class<T> cls) {
        Gson gson = new Gson();
        return gson.fromJson(data, cls);
    }

    public static <T> T decodeJson(String data, Type type) {
        Gson gson = new Gson();
        return gson.fromJson(data, type);
    }

    public static String encodeJson(Object o) {
        Gson gson = new Gson();
        return gson.toJson(o);
    }


}
