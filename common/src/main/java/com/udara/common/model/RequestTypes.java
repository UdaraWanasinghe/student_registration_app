package com.udara.common.model;

public class RequestTypes {
    public static final int CREATE_USER_REQUEST = 1;
    public static final int GET_USER_BY_NAME_REQUEST = 2;
    public static final int GET_ALL_USERS_REQUEST = 3;
    public static final int DELETE_USER_REQUEST = 4;
    public static final int EDIT_USER_REQUEST = 5;
}
