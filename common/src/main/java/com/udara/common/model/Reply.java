package com.udara.common.model;

public class Reply {
    // status code of the reply
    private int statusCode;

    // message of the reply
    private String message;

    // content of the reply
    // may be null for some reply
    // in json format
    private String content;

    public Reply() {

    }

    public Reply(int statusCode, String message, String content) {
        this.statusCode = statusCode;
        this.message = message;
        this.content = content;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
