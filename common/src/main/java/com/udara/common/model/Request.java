package com.udara.common.model;

public class Request {
    private int type;
    private String content;

    public Request() {
    }

    public Request(int type, String content) {
        this.type = type;
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public Request setType(int type) {
        this.type = type;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Request setContent(String content) {
        this.content = content;
        return this;
    }
}
