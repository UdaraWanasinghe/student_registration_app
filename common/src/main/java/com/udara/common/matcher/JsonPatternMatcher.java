package com.udara.common.matcher;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.udara.common.pattern.JsonPattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonPatternMatcher implements PatternMatcherInterface {
    private Pattern mPattern = Pattern.compile("\\{(.)*\\}");

    @Override
    public JsonElement getJson(String str) {
        JsonParser jsonParser = new JsonParser();
        Matcher matcher = mPattern.matcher(str);
        if (matcher.find()) {
            try {
                System.out.println("Parsing json");
                JsonElement js =jsonParser.parse(matcher.group());
                System.out.println("PArsing done");
                return js;
            } catch (JsonParseException ignored) {
                System.out.println("parse failed");
            }
        }else{
            System.out.println("pattern not found");
        }
        return null;
    }
}
