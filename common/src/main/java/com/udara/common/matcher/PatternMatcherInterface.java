package com.udara.common.matcher;

import com.google.gson.JsonElement;
import com.udara.common.pattern.JsonPattern;

public interface PatternMatcherInterface {
    JsonElement getJson(String str);
}
