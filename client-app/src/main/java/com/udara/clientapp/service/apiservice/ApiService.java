package com.udara.clientapp.service.apiservice;

import com.udara.clientapp.service.clientservice.ClientService;
import com.udara.common.codec.JsonCodec;
import com.udara.common.model.Reply;
import com.udara.common.model.Request;
import com.udara.common.model.RequestTypes;
import com.udara.common.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class ApiService {
    private ClientService clientService;

    public ApiService() {
        clientService = new ClientService();
    }

    private static Logger logger = LogManager.getLogger(ApiService.class);

    public Reply createUser(User user) {
        try {
            logger.info("Creating user");
            String userEncoded = JsonCodec.encodeJson(user);
            logger.info("Encoded data: " + userEncoded);
            Request request = new Request()
                    .setType(RequestTypes.CREATE_USER_REQUEST)
                    .setContent(userEncoded);
            logger.info("Connecting to server");
            clientService.connect();
            logger.info("Connection success");
            String requestEncoded = JsonCodec.encodeJson(request);
            logger.info("Request: " + requestEncoded);
            clientService.write(requestEncoded.getBytes());
            logger.info("writing done");
            String received = clientService.read();
            logger.info("read complete");
            clientService.close();
            logger.info("close complete");
            return JsonCodec.decodeJson(received, Reply.class);
        } catch (IOException e) {
            try {
                clientService.close();
            } catch (IOException ignored) {

            }
            return new Reply(0, e.getMessage(), null);
        }
    }

    public void getUser() {

    }

    public void editUser() {

    }

    public Reply deleteUser(String username) {
        try {
            Request request = new Request()
                    .setType(RequestTypes.DELETE_USER_REQUEST)
                    .setContent(username);
            String encodedRequest = JsonCodec.encodeJson(request);
            clientService.connect();
            clientService.write(encodedRequest.getBytes());
            String encodedReply = clientService.read();
            clientService.close();
            return JsonCodec.decodeJson(encodedReply, Reply.class);
        } catch (IOException e) {
            return new Reply(0, e.getMessage(), null);
        }
    }

    public Reply getAllUsers() {
        try {
            Request request = new Request()
                    .setType(RequestTypes.GET_ALL_USERS_REQUEST);
            String requestEncoded = JsonCodec.encodeJson(request);
            clientService.connect();
            clientService.write(requestEncoded.getBytes());
            String str = clientService.read();
            clientService.close();
            return JsonCodec.decodeJson(str, Reply.class);
        } catch (IOException e) {
            return new Reply(0, e.getMessage(), null);
        }
    }

    public Reply getUserByUsername(String username) {
        try {
            clientService.connect();
            Request request = new Request()
                    .setType(RequestTypes.GET_USER_BY_NAME_REQUEST)
                    .setContent(username);
            String encodedRequest = JsonCodec.encodeJson(request);
            clientService.write(encodedRequest.getBytes());
            String replyString = clientService.read();
            clientService.close();
            return JsonCodec.decodeJson(replyString, Reply.class);
        } catch (IOException e) {
            return new Reply(0, e.getMessage(), null);
        }
    }
}
