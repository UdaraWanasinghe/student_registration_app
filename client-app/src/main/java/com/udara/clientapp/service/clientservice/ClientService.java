package com.udara.clientapp.service.clientservice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class ClientService implements ClientServiceInterface {
    private static final String SERVER_IP = "127.0.0.1";

    private static final int SERVER_PORT = 4099;

    private static Logger logger = LogManager.getLogger(ClientService.class);

    private Socket socket;
    private BufferedWriter bufferedWriter;
    private BufferedReader bufferedReader;

    public ClientService() {
    }

    @Override
    public void connect() throws IOException {
        if (isConnected()) {
            logger.debug("Client is already connected to: " + socket.getInetAddress().toString());
        } else {
            logger.debug("Connecting to server");
            socket = new Socket(SERVER_IP, SERVER_PORT);
            logger.debug("Server connection success");
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        }
    }

    @Override
    public String read() throws IOException {
        String str = bufferedReader.readLine();
        str = str.substring(str.indexOf('{'), str.lastIndexOf('}') + 1);
        System.out.println("Received: " + str);
        return str;
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        if (isConnected()) {
            bufferedWriter.write(new String(bytes) + "\r\n");
            bufferedWriter.flush();
        } else {
            logger.debug("Not connected to the server");
        }
    }

    @Override
    public void close() throws IOException {
        if (isConnected()) {
            socket.close();
        } else {
            logger.debug("Connection is already closed");
        }
        socket = null;
    }

    /**
     * check whether there is an active connection
     *
     * @return true if there is an ongoing connection, else false
     */
    private boolean isConnected() {
        return socket != null && socket.isConnected() && !socket.isClosed();
    }

}
