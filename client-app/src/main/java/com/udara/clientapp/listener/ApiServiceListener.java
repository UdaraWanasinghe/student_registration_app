package com.udara.clientapp.listener;

/**
 * Api callback listeners
 * @param <S> Type of success message
 * @param <E> Type of failure message
 */
public interface ApiServiceListener<S, E> {
    public void onSuccess(S s);

    public void onFailed(E e);
}
