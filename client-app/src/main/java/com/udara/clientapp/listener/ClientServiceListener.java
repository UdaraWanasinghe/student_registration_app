package com.udara.clientapp.listener;

public interface ClientServiceListener {
    public void onReply(byte[] data);

    public void onError(String err);
}
