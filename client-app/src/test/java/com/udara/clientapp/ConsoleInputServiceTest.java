package com.udara.clientapp;

//import com.udara.clientappspring.service.inputservice.ConsoleInputService;
//import com.udara.common.model.User;
//import org.junit.Assert;
//import org.junit.Ignore;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@RunWith(SpringRunner.class)
//@TestPropertySource("classpath:application.properties")
//@ContextConfiguration("classpath:spring-context.xml")
//@Ignore
//public class ConsoleInputServiceTest {
//    @Autowired
//    private ConsoleInputService consoleInputService;
//
//    @Test
//    public void validMethodTest() {
//        String id = "164143D";
//        String name = "Udara";
//        String email = "udara.mobile@gmail.com";
//        String dob = "1996-07-28";
//        String address = "My addr";
//
//        User user = new User();
//        user.setId(id);
//        user.setUsername(name);
//        user.setEmail(email);
//        user.setDob(dob);
//        user.setAddress(address);
//
//        Assert.assertTrue(consoleInputService.isValid(user));
//
//        user.setEmail("164143");
//        Assert.assertFalse(consoleInputService.isValid(user));
//        user.setDob("164143D");
//
//        user.setEmail("mail");
//        Assert.assertFalse(consoleInputService.isValid(user));
//        user.setDob("udara.mobile@gmail.com");
//
//        user.setDob("2311");
//        Assert.assertFalse(consoleInputService.isValid(user));
//        user.setDob("1996-07-28");
//
//    }
//}
