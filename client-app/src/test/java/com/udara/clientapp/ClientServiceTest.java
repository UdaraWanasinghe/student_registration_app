package com.udara.clientapp;

import com.udara.clientapp.service.clientservice.ClientService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

import java.io.IOException;
import java.net.ServerSocket;
//
//@Ignore
//public class ClientServiceTest {
//    @Value("${server.port}")
//    private int PORT;
//
//    private ServerSocket serverSocket;
//
//    @Autowired
//    private ClientService clientService;
//
//    private Logger logger = LogManager.getLogger(ClientServiceTest.class);
//
//    @Before
//    public void startServer() throws IOException, InterruptedException {
//        serverSocket = new ServerSocket(PORT);
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    serverSocket.accept();
//                    while (!serverSocket.isClosed()) ;
//                    logger.debug("Server socket closed");
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
//        Thread.sleep(200);
//    }
//
//    @Test
//    public void clientConnectionTest() throws IOException {
//        Assert.assertNotNull(clientService);
//        clientService.connect();
//    }
//
//    @After
//    public void cleanUp() throws IOException {
//        clientService.close();
//        serverSocket.close();
//    }
//}
