package com.udara.serverapp;

import com.udara.serverapp.service.ServerService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

import java.io.IOException;

/**
 * Server App
 */

@SpringBootApplication
@ImportResource("classpath:spring-context.xml")
public class ServerApp {
    public static void main(String[] args) throws IOException {
        ApplicationContext applicationContext = SpringApplication.run(ServerApp.class, args);
        ServerService serverService = applicationContext.getBean(ServerService.class);
        serverService.start();
    }
}