package com.udara.serverapp.handler;


import com.udara.common.codec.JsonCodec;
import com.udara.common.model.Reply;
import com.udara.common.model.Request;
import com.udara.common.model.RequestTypes;
import com.udara.common.model.User;
import com.udara.serverapp.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class RequestHandler implements Runnable {
    private Logger logger = LogManager.getLogger(RequestHandler.class);

    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;

    private UserService userService;

    public RequestHandler() {

    }

    public RequestHandler(Socket socket, UserService userService) throws IOException {
        this.socket = socket;
        this.userService = userService;
        this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    private void readRequest() throws IOException {
        String str = bufferedReader.readLine();
        str = str.substring(str.indexOf('{'), str.lastIndexOf('}') + 1);
        Request request = JsonCodec.decodeJson(str, Request.class);
        logger.info("Request type: " + request.getType());
        if (request.getType() != 0) {
            switch (request.getType()) {
                case RequestTypes.CREATE_USER_REQUEST:
                    User user1 = JsonCodec.decodeJson(request.getContent(), User.class);
                    boolean isSaved = userService.addUser(user1);
                    if (isSaved) {
                        sendReply(201, "User creation successful");
                    } else {
                        sendReply(500, "User creation failed");
                    }
                    break;

                case RequestTypes.GET_USER_BY_NAME_REQUEST:
                    User user2 = userService.getUserByUsername(request.getContent());
                    if (user2 != null) {
                        String content = JsonCodec.encodeJson(user2);
                        sendReply(200, "User found", content);
                    } else {
                        sendReply(404, "User not found");
                    }
                    break;

                case RequestTypes.GET_ALL_USERS_REQUEST:
                    List<User> userList = userService.getAllUsers();
                    String content = JsonCodec.encodeJson(userList);
                    sendReply(200, "Success", content);
                    break;

                case RequestTypes.DELETE_USER_REQUEST:
                    String username = request.getContent();
                    logger.info("deleting user: " + username);
                    userService.deleteUserByUsername(username);
                    logger.info("User delete success");
                    sendReply(200, "User deleted");
                    break;

                default:
                    sendReply(400, "Unknown request");
            }
        } else {
            sendReply(400, "Invalid json");
        }
        close();
    }

    private void write(String str) throws IOException {
        bufferedWriter.write(str + "\r\n");
        bufferedWriter.flush();
        logger.debug("Reply sent: " + str);
    }

    public void run() {
        try {
            readRequest();
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void sendReply(int statusCode, String message, String content) throws IOException {
        Reply reply = new Reply(statusCode, message, content);
        String encodedReply = JsonCodec.encodeJson(reply);
        write(encodedReply);
        logger.info("Reply sent: " + message);
    }

    private void sendReply(int statusCode, String message) throws IOException {
        sendReply(statusCode, message, null);
    }

    private void close() throws IOException {
        if (socket != null && !socket.isClosed()) {
            socket.close();
            logger.info("Connection closed");
        } else {
            logger.info("Connection already closed");
        }
    }
}
