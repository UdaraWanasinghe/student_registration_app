package com.udara.serverapp.service;

import java.io.IOException;
import java.net.Socket;

public interface ServerServiceInterface {

    public void start() throws IOException;

    public void close() throws IOException;

    public void handleConnection(Socket socket) throws IOException;
}
