package com.udara.serverapp.service;

import com.udara.common.model.User;
import com.udara.serverapp.entity.Student;
import com.udara.serverapp.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserService {
    private static Logger logger = LogManager.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByUsername(String username) {
        Optional<Student> studentObject = userRepository.findByUsername(username);
        if (studentObject.isPresent()) {
            Student student = studentObject.get();
            logger.info("User present: " + student.getUsername());
            User user = new User();
            user.setUsername(student.getUsername());
            user.setPassword(student.getPassword());
            user.setRole(student.getRole().name());
            return user;
        } else {
            logger.info("User not present for username: " + username);
        }
        return null;
    }

    public boolean addUser(User user) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Student student = new Student();
        student.setId(user.getId());
        student.setEmail(user.getEmail());
        student.setUsername(user.getUsername());
        student.setPassword(user.getPassword());
        try {
            student.setDob(simpleDateFormat.parse(user.getDob()));
        } catch (ParseException e) {
            logger.error(e.getMessage());
            return false;
        }
        student.setAddress(user.getAddress());
        userRepository.save(student);
        return true;
    }

    public List<User> getAllUsers() {
        Iterable<Student> students = userRepository.findAll();
        List<User> userList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (Student student : students) {
            User user = new User();
            user.setUsername(student.getUsername());
            user.setEmail(student.getEmail());
            String dob = sdf.format(student.getDob());
            user.setDob(dob);
            user.setId(student.getId());
            userList.add(user);
        }
        return userList;
    }

    public boolean updateUser(User user) {
        return false;
    }

    public void deleteUserByUsername(String username) {
        userRepository.deleteById(username);
    }
}
