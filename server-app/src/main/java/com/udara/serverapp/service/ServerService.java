package com.udara.serverapp.service;

import com.udara.serverapp.handler.RequestHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerService implements ServerServiceInterface {
    @Value("${server.port}")
    private int SERVER_PORT;

    @Value("${executor.threadpoolsize}")
    private int THREAD_POOL_SIZE;

    private static Logger logger = LogManager.getLogger(ServerService.class);

    private ServerSocket mServerSocket;
    private ExecutorService mExecutorService;

    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PostConstruct
    public void initialize() {
        createNewExecutorService();
    }

    @Override
    public void start() throws IOException {
        logger.info("Starting server");
        mServerSocket = new ServerSocket(SERVER_PORT);
        logger.info("Waiting for connection");
        while (isConnected()) {
            Socket socket = mServerSocket.accept();
            logger.info("Connection received: " + socket.getInetAddress().toString());
            this.handleConnection(socket);
        }
    }

    @Override
    public void close() throws IOException {
        if (isConnected()) {
            mServerSocket.close();
        }
    }

    @Override
    public void handleConnection(Socket socket) throws IOException {
        RequestHandler requestHandler = new RequestHandler(socket, userService);
        mExecutorService.submit(requestHandler);
    }

    private void createNewExecutorService() {
        stopCurrentExecutors();
        mExecutorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
    }

    private void stopCurrentExecutors() {
        if (mExecutorService != null && !mExecutorService.isShutdown()) {
            mExecutorService.shutdown();
        }
    }

    public boolean isConnected() {
        return mServerSocket != null && !mServerSocket.isClosed();
    }
}
