package com.udara.serverapp.service;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyServerService {
    public void run(){
        // multithreaded event group that handles IO operations
        // two event loop groups should be defined
        // one is to handle incoming connection
        // other one is to handle traffic of the accepted connection
        // number of threads are configurable via constructor
        NioEventLoopGroup  bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup  workerGroup = new NioEventLoopGroup();

        try{
            // ServerBootstrap is a helper class to setup the server
            // also can setup server using channel. but it's very difficult
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    // specify new channel to accept incoming connection
                    .channel(NioServerSocketChannel.class)
                    // newly accepted channel will be evaluated here
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {

                        }
                    });

        }finally {

        }

    }
}
