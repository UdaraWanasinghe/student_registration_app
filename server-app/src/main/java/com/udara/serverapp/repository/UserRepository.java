package com.udara.serverapp.repository;

import com.udara.serverapp.entity.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<Student, String> {
    @Query("SELECT s FROM Student s WHERE s.username = ?1")
    Optional<Student> findByUsername(String username);

//    @Query("DELETE FROM Student WHERE Student.username = ?1")
//    void deleteByUsername(String username);
}
