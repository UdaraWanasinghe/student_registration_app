package com.udara.serverapp.entity.enums;

public enum Role {
    ROLE_ADMIN,
    ROLE_USER
}
