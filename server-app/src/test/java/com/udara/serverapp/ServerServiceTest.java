package com.udara.serverapp;

import com.udara.serverapp.service.ServerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.Socket;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = {"classpath:application.properties"})
@ContextConfiguration(locations = {"classpath:spring-context.xml"})
@Ignore
public class ServerServiceTest {
    private Logger logger = LogManager.getLogger(ServerServiceTest.class);

    @Autowired
    private ServerService serverService;

    @Value("${server.port}")
    private int PORT;

    @Value("${server.host}")
    private String HOST;

    @Before
    public void initialize() throws InterruptedException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serverService.start();
                } catch (IOException e) {
                    Assert.fail(e.getMessage());
                }
            }
        }).start();
        while (!serverService.isConnected()) {
            logger.debug("Waiting for server to start");
            Thread.sleep(1000);
        }
    }

    @Test
    public void serverConnectionTest() throws IOException {
        Socket socket = new Socket(HOST, PORT);
        Assert.assertTrue(socket.isConnected());
    }

    @After
    public void cleanUp() throws IOException {
        serverService.close();
    }
}
