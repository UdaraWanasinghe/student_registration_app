package com.udara.serverapp;

import com.udara.serverapp.entity.Student;
import com.udara.serverapp.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@TestPropertySource("classpath:application.properties")
@Ignore
public class DatabaseServiceTest {
    private Logger logger = LogManager.getLogger(DatabaseServiceTest.class);

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void databaseSaveTest() throws ParseException {
        assertThat(entityManager).isNotNull();
        assertThat(userRepository).isNotNull();

        String id = "164143D";
        String name = "Udara";
        String email = "udara.mobile@gmail.com";
        String dob = "1996-07-28";
        String address = "My addr";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Student student = new Student();
        student.setId(id);
        student.setUsername(name);
        student.setEmail(email);
        student.setDob(simpleDateFormat.parse(dob));
        student.setAddress(address);
        userRepository.save(student);

        Optional<Student> oStd = userRepository.findById(student.getId());

        if (oStd.isPresent()) {
            Student studentRetrieved = oStd.get();
            logger.debug(studentRetrieved.toString());
            Assert.assertEquals(id, studentRetrieved.getId());
            Assert.assertEquals(name, studentRetrieved.getUsername());
            Assert.assertEquals(email, studentRetrieved.getEmail());
            Assert.assertEquals(dob, simpleDateFormat.format(studentRetrieved.getDob()));
            Assert.assertEquals(address, studentRetrieved.getAddress());
        } else {
            Assert.fail("Student not found");
        }
    }
}
