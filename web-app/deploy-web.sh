mvn clean install
retVal=$?
if [[ ${retVal} -eq 0 ]]; then
  sh /hms/installs/apache-tomcat-9.0.24/bin/shutdown.sh
  rm -r /hms/installs/apache-tomcat-9.0.24/webapps/web-app.war
  rm -rf /hms/installs/apache-tomcat-9.0.24/webapps/web-app/
  cp target/web-app.war /hms/installs/apache-tomcat-9.0.24/webapps/
  gnome-terminal -e "sh /hms/installs/apache-tomcat-9.0.24/bin/catalina.sh run"
fi
