<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: udara
  Date: 9/23/19
  Time: 9:57 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
        RegApp
    </a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/register">Register</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/all-users">View All</a>
        </li>
    </ul>
</nav>
<div class="d-flex justify-content-center my-5 w-100">
    <div class="card w-50">
        <div class="card-body">
            <h5 class="card-title">Login</h5>
        </div>
        <div class="card-body">
            <c:if test="${message != null}">
                <div class="alert alert-primary" role="alert">
                        ${message}
                </div>
            </c:if>
            <form name="f" action="${pageContext.request.contextPath}/login" method="POST">
                <div>
                    <label for="inputUsername">Username</label>
                    <input type="text" id="inputUsername" placeholder="Enter username" class="form-control"
                           name="username">
                </div>
                <div class="form-group mt-4">
                    <label for="inputPassword">Password</label>
                    <input type="password" id="inputPassword" placeholder="Enter password" class="form-control"
                           name="password">
                </div>
                <button class="btn btn-primary btn-block text-center mt-4" type="submit">Login</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>