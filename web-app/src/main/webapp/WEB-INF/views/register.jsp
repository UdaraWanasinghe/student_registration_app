<%--
  Created by IntelliJ IDEA.
  User: udara
  Date: 2019-09-18
  Time: 15.51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
        RegApp
    </a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/register">Register</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/all-users">View All</a>
        </li>
    </ul>
</nav>
<div class="d-flex justify-content-center my-5 w-100">
    <div class="card w-50">
        <div class="card-body">
            <h5 class="card-title">Register</h5>
        </div>
        <div class="card-body">
            <c:if test="${message != null}">
                <div class="alert alert-primary" role="alert">
                        ${message}
                </div>
            </c:if>
            <form action="${pageContext.request.contextPath}/register" method="post">
                <div class="form-group">
                    <label for="inputId">Index no</label>
                    <input type="text" id="inputId" placeholder="Enter index no" class="form-control" name="id">
                </div>
                <div class="form-group">
                    <label for="inputUsername">Username</label>
                    <input type="text" id="inputUsername" placeholder="Enter username" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label for="inputEmail">Email</label>
                    <input type="text" id="inputEmail" placeholder="Enter email" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label for="inputNIC">NIC</label>
                    <input type="text" id="inputNIC" placeholder="Enter NIC" class="form-control" name="nic">
                </div>

                <div class="form-group">
                    <label for="inputDOB">DOB</label>
                    <input type="date" id="inputDOB" placeholder="Enter DOB" class="form-control" name="dob">
                </div>

                <div class="form-group">
                    <label for="inputAddress">Address</label>
                    <input type="text" id="inputAddress" placeholder="Enter Address" class="form-control"
                           name="address">
                </div>

                <div class="form-group">
                    <label for="inputPassword">Password</label>
                    <input type="password" id="inputPassword" placeholder="Enter password" class="form-control"
                           name="password">
                </div>
                <div class="form-group">
                    <label for="inputConfirmPassword">Confirm Password</label>
                    <input type="password" id="inputConfirmPassword" placeholder="Enter Password Again"
                           class="form-control">
                </div>
                <button class="btn btn-primary btn-block text-center mt-4" type="submit">Register</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
