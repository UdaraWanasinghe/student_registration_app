<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <a class="navbar-brand" href="#">
        RegApp
    </a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/register">Register</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/admin/all-users">View All</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/logout">Logout</a>
        </li>
    </ul>
</nav>
<c:if test="${userList != null}">
    <ul class="list-group m-5">
        <c:forEach items="${userList}" var="user">
            <li class="list-group-item">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">${user.username}</h5>
                        <h6 class="card-subtitle mb-2 text-muted">${user.email}</h6>
                        <h6 class="card-subtitle mb-2 text-muted">${user.id}</h6>
                        <h6 class="card-subtitle mb-2 text-muted">${user.nic}</h6>
                        <h6 class="card-subtitle mb-2 text-muted">${user.dob}</h6>
                    </div>
                    <a class="card-link m-4"
                       href="${pageContext.request.contextPath}/admin/delete-user/${user.id}">Delete</a>
                </div>
            </li>
        </c:forEach>
    </ul>
</c:if>
</body>
</html>