package com.udara.webapp.controller;

import com.google.gson.reflect.TypeToken;
import com.udara.clientapp.service.apiservice.ApiService;
import com.udara.common.codec.JsonCodec;
import com.udara.common.model.Reply;
import com.udara.common.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.Type;
import java.util.List;

@Controller
public class StudentController {
    private static Logger logger = LogManager.getLogger(StudentController.class);

    private ApiService apiService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public void setApiService(ApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }

    @GetMapping("/register")
    public String getRegisterForm() {
        return "register";
    }

    @PostMapping("/register")
    public ModelAndView register(
            @RequestParam String id,
            @RequestParam String name,
            @RequestParam String email,
            @RequestParam String nic,
            @RequestParam String dob,
            @RequestParam String address,
            @RequestParam String password,
            Model model
    ) {
        logger.info("registration post request received");

        final User user = new User();
        user.setId(id);
        user.setUsername(name);
        user.setEmail(email);
        user.setNic(nic);
        user.setDob(dob);
        user.setAddress(address);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        Reply reply = apiService.createUser(user);
        model.addAttribute("message", reply.getMessage());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("register");
        model.addAttribute("message", reply.getMessage());
        return modelAndView;
    }

    @GetMapping("/home")
    public String getLoginForm() {
        return "home";
    }

    @GetMapping("/admin/all-users")
    public ModelAndView getAllUsers() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("all-users");
        Reply reply = apiService.getAllUsers();
        if (reply.getStatusCode() == 200) {
            Type type = new TypeToken<List<User>>() {
            }.getType();
            List<User> userList = JsonCodec.decodeJson(reply.getContent(), type);
            mav.addObject(userList);
        }
        return mav;
    }

    @GetMapping("/admin/delete-user/{username}")
    public ModelAndView deleteUser(@PathVariable String username) {
        apiService.deleteUser(username);
        return getAllUsers();
    }
}
