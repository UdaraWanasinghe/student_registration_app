package com.udara.webapp.service.userinfoservice;

import com.udara.clientapp.service.apiservice.ApiService;
import com.udara.clientapp.service.clientservice.ClientService;
import com.udara.common.codec.JsonCodec;
import com.udara.common.model.Reply;
import com.udara.common.model.Request;
import com.udara.common.model.RequestTypes;
import com.udara.common.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserInfoService implements UserDetailsService {
    private ApiService apiService;

    private static Logger logger = LogManager.getLogger(UserInfoService.class);

    @Autowired
    public void setApiService(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("Finding user: " + username);
        Reply reply = apiService.getUserByUsername(username);
        if (reply.getStatusCode() == 200) {
            User user = JsonCodec.decodeJson(reply.getContent(), User.class);
            logger.info("User found: " + user.getUsername());
            return new UserInfo(user);
        } else {
            logger.info("Error retrieving user: " + reply.getMessage());
            throw new UsernameNotFoundException("User not found for name: " + username);
        }
    }

}
